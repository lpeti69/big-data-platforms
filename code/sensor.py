import sys
import csv
from time import sleep
from json import dumps, loads
from kafka import KafkaProducer
from threading import Timer

broker_ip, broker_port = sys.argv[1:]
producer = KafkaProducer(
    bootstrap_servers=[broker_ip + ':' + broker_port],
    value_serializer=lambda x: dumps(x).encode('utf-8')
)

record_count = 0
window = 10

def show_progress():
    global record_count
    log = "Records ingested: {} in {} seconds".format(record_count, window)
    print(log)
    with open("ingestor_app_emulator.log", "a") as text_file:
        print(log, file=text_file)

    t = Timer(window, show_progress)
    t.start()
    record_count = 0

t = Timer(window, show_progress)
t.start()

with open('../data/Indoor_Location_Detection_Dataset.csv', 'r') as csv_file:
    reader = csv.DictReader(csv_file)
    for record in reader:
        producer.send('client1', value=record)
        record_count += 1
        sleep(0.1)
    t.cancel()