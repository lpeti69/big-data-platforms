import os
import sys
import json
from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils, Broker
from kafka import KafkaProducer
from time import time
from datetime import datetime


start_time = time()
time_window = 1

class Analytics:
    def __init__(self, window):
        self.window_ = window
        self.user_summary_ = dict()
        self.timing_summary_ = dict()
        self.counter_ = 0

    def update_room(self, time_, msg, producer):
        result = msg.collect()
        for data in result:
            user, room, count = data[0][0], data[0][1], data[1]
            empty = [0 for i in range(self.window_)]
            if user not in self.user_summary_:
                self.user_summary_[user] = {room: empty}
                self.user_summary_[user][room][0] = count
            else:
                us = self.user_summary_[user]
                if room not in us:
                    us[room] = empty
                    us[room][0] = count
                else:
                    us[room][self.counter_] = count
            self.counter_ = (self.counter_ + 1) % self.window_
        summary = self.get_room_results()
        handler(summary, producer)
        return msg

    def get_room_results(self):
        result = dict()
        for user, room_info in self.user_summary_.items():
            result[user] = dict(summary = 0)
            summary = 0
            for room, counts in room_info.items():
                room_res = sum(counts)
                result[user][room] = room_res
            summary += room_res
            result[user]['summary'] = summary / float(len(result[user]))
        return result

    def update_timing(self, time_, msg, producer):
        result = msg.collect()
        for data in result:
            user, avg = data[0], time_window / float(data[1])
            if user not in self.timing_summary_:
                self.timing_summary_[user] = avg
            else:
                self.timing_summary_[user] = (self.timing_summary_[user] + avg) / 2
        summary = self.timing_summary_
        handler(summary, producer)
        return msg


analytics = Analytics(window = 15)

def analyze(stream, producer):
    rdd = stream \
        .map(lambda msg: json.loads(msg[1])) \
        .map(lambda record: ((record['part_id'], record['room']), 1)) \
        .reduceByKey(lambda x, y: x + y) \

    rdd \
        .foreachRDD(lambda time, data: analytics.update_room(time, data, producer))
    
    rdd \
        .map(lambda record: (record[0][0], record[1])) \
        .reduceByKey(lambda x, y: x + y) \
        .foreachRDD(lambda time, data: analytics.update_timing(time, data, producer))

    rdd.pprint()

def handler(data, producer):
    global start_time
    print(data)
    producer.send('client1-analytics', value=data)
    end_time = time()

    log = 'Elapsed time: {}s\n'.format(end_time - start_time)
    print(log)
    with open('analytics.log', 'a') as text_file:
        text_file.write(log)

    start_time = time()
    return data

if __name__ == "__main__":
    spark = SparkSession.builder \
        .appName('CustomerStreamingApp') \
        .getOrCreate()
    spark.sparkContext.setLogLevel('WARN')
    ssc = StreamingContext(spark.sparkContext, time_window)

    topics = ['client1']
    ##broker_ip, broker_port = '130.233.85.86', '32789'
    broker_ip, broker_port = sys.argv[1:]
    broker = broker_ip + ':' + broker_port
    producer = KafkaProducer(
        bootstrap_servers=[broker],
        value_serializer=lambda x: json.dumps(x).encode('utf-8')
    )

    kafka_stream = KafkaUtils.createDirectStream(
        ssc, 
        topics,
        {'metadata.broker.list': broker}
    )

    start_time = time()
    analyze(kafka_stream, producer)

    ssc.start()
    ssc.awaitTermination()