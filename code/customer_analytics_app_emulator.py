import sys
from json import loads
from kafka import KafkaConsumer

broker_ip, broker_port = sys.argv[1:]
#broker_ip, broker_port = '130.233.85.86', '32788'

consumer = KafkaConsumer(
    'client1-analytics',
    bootstrap_servers=[broker_ip + ':' + broker_port],
    enable_auto_commit=True,
    value_deserializer=lambda x: loads(x.decode('utf-8'))
)

for msg in consumer:
    msg = msg.value
    print('Received analytics result: {}'.format(msg))
    with open('client_statistics.log', 'a') as text_file:
        print(msg, file=text_file)