# Design report


## 1
1.1

Dataset: Indoor Localization Dataset

The dataset contains information concerning the older people’s movement inside their homes regarding their indoor location in the home setting.
The dataset is recorded daily with the use of smart beacon devices installed in each older person's home and monitored through the system.
Each record of the dataset has the following fields:
- **part_id**: The user ID, which should be a 4-digit number
- **ts_date**: The recording date, which follows the “YYYYMMDD” format, e.g. 14 September 2017, is formatted as 20170914
- **ts_time**: The recording time, which follows the “hh:mm:ss” format
- **room**: The room which the person entered on the specific date and time (It is assumed that the person remained in the room till the next recording of the same day)

Analytics
(Indoor tracking app)
Streaming
- Average elapsed time between location changes
- The same compared to population average(1)
- How much time did the person spent in the last room
- How much time did a person spend in each room recently(2)
- Train ML to learn to predict how long are you going to stay in one place (learns people behaviour)

Batch
1. What's your favourite room (the one in which you spend the most of the time)
   Based on (2), Accumulate the streaming window's
2. Calculating population's average (1)

1.2

(i) should the analytics handle keyed or non-keyed data streams for the customer data, and 
- Although the data stream does not provide key it is not necessary since the (customer_id, data_time) is a composite primary key,
  which can be considered as a key. By this, I could check if I have already received a record or not, making the streaming exactly once deliver, i.e analyize each record only once.

(ii) which types of delivery guarantees should be suitable.
- primary: exactly once (we do not want to send back duplicates, or miss one, both of which might confuse the customers)
- secondary: in order (it would create false results for our analytics, by missing a motion detection timestemp, i.e. receiving it out of order)

1.3

(i) which types of time should be associated with stream sources for the analytics and be considered in stream processing (if the data sources have no timestamps associated with events, then what would be your solution)

Particularly in this dataset, the entering time for a room should be considered since ideally the sensor should detect the motion real time and
send it immediately.

(ii) which types of windows should be developed for the analytics (if no window, then why). Explain these aspects and give examples.

Sliding window should be used, since:
1. average time could be calculated by running average, in which we can keep up the window average has a time complexity of O(1).
2. We do not need any window for providing the amount of time a customer has spent in the last room
3. Finally the sliding window is also a very good way to keep tracking the distribution of the spent in the rooms 


1.4

Explain which performance metrics would be important for the streaming analytics for your customer cases.
- Throughput (efficiency) - primary
    Because there might be a large amount of users
    And they could also change locations frequently
    therefore we should have high throughput rate (some delay is acceptable - less important)
- The latency is less important, because it is not big of a deal if we should our analytics to our customers just only after a couple of seconds he/she has changed his/her position.

1.5
Provide a design of your architecture for the streaming analytics service in which you clarify:
customer data sources, mysimbdp message brokers, mysimbdp streaming computing
service, customer streaming analytics app, mysimbdp-coredms, and other components, if
needed. Explain your choices of technologies for implementing your design and reusability of
existing assignment works. Note that the result from customerstreamapp will be sent back
to the customer in near real-time.

Reusability:
I have not reused anything, since I wanted to experience how does it look like to create and manage every component locally without costly cloud providers such as GCP. What is more, in the end, you can still deploy each component individually among different nodes/clusters/servers since they are not dependent on each other.

Choice of technologies:
I have picked spark over flink since
1. our solution prefers handling large data sizes efficiently over providing the results immediately
   Nonetheless, we would not like to send them back with too big delay since it might kill the customer's experience.
   Therefore, I have not chosen flink, but apache with it's streaming capabilities (micro-batching).

Kafka - distributed streaming platform
1. Native connection with spark, meaning that I do not have to make unnecessary transformation between the message queue's data the streaming analytics
    For example with rabbitMQ, I would have to transform thourgh several steps the incoming json file(representing one record) to pandas dataframe then to spark dataframe. Kafka with spark, instead, supports this natively, I can immediately implement callbacks as topic subscriptions.
2. Because of the fact that it is distributed, it is easily scalable and handle larger size of data in production mode more efficiently

![](bdp3.jpg)


## 3
3.1

If you would like the analytics results to be stored also into mysimbdp-coredms as the final
sink, how would you modify the design and implement this (better to use a figure to explain your design)

I would change mysimbdp-coredms from mongodb to the hdfs itself, and I would store the results locally with the streaming app, so that later they will be more accesisable

![](bdp3_3_1.jpg)

3.2

Given the output of streaming analytics stored in mysimbdp-coredms for a long time.
Explain a batch analytics (see also Part 1, question 1) that could be used to analyze such
historical data. How would you implement it?

As I have mentioned previously, we could aggregate the customer's location distribution data to form the customer's preference ranking, i.e. which places are loved the most, loved the second most and so forth.

I would store the stream analytics on hdfs, then after the end of week or month I would start a large job my analyzing that aggregating that periods data with spark sql and spark ml. The latter can also be done by Apache Hive, it just a matter of preference. Technically I would setup a timer which should be triggered at the end of each episode. Then it would submit another spark job called 'bach-analytics', whom would send the results to the customer as stastics after it has finished.

3.3

Assume that the streaming analytics detects a critical condition (e.g., a very high rate of
alerts) that should trigger the execution of a batch analytics to analyze historical data. How
would you extend your architecture in Part 1 to support this (use a figure to explain your work)?

If the streaming analytics, for example, receives location changes too frequently, an anomaly detector (as port of the streaming analytics) might detect that a certain sensor is unreliable and has to be repared.
If it is detected, change with batch analytics if it has been wrong throughout a longer period or there might be just guests. 

![](bdp3_3_3.jpg)

3.4

If you want to scale your streaming analytics service for many customers and data, which
components would you focus and which techniques you want to use?
1. First and foremost, I would focus on improving by making more elastic the underlying infrastructure, i.e. that's the most important building block for every big data platform. In the current solution I use a spark standalone cluster (simulated with one node) which would not be able to withstand the real-world demanding veracity/velocity/volume. 
Therefore I would change it to deploy it kubernetes since:
    1. it is more flexible considering these metrics
    2. I have already dockerized kafka and probably would also do the same with the machine learning analytics (which would probably necessary for the increased data size). Kubernetes is the state-of-the-art platform currently for dockerizing.
2. Scale kafka by creating more brokers and topics with replication (so we do not lose data) with sharding
   Not to mention that it is also quite straightfoward

3.5

Is it possible to achieve end-to-end exactly once delivery in your current implementation? If
yes, explain why. If not, what could be conditions and changes to make it happen? If it is
impossible to have end-to-end exactly once delivery in your view, explain why

No, because it is only exactly once between kafka and the streaming analytics, the other parts do not support fault-tolerance. It is impossible to achieve it, since the sensors might be unreliable, thus they might send the same data multiple times, especially if they are using udp connection instead of tcp.