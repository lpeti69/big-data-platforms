#!/bin/bash
SPARK_FILENAME=spark_hadoop.tgz
SPARK=resources/spark

## INSTALLING PYTHON DEPENDENCIES
python3 -m venv venv
source venv/bin/activate
sleep 1
pip3 install -r requirements.txt

## INSTALLING SPARK
mkdir resources
wget -O resources/$SPARK_FILENAME http://mirror.netinch.com/pub/apache/spark/spark-2.4.4/spark-2.4.4-bin-hadoop2.7.tgz 
tar zxvf resources/$SPARK_FILENAME -C resources
SPARK_DIR=$(find resources/* -maxdepth 0 -type d)
echo $SPARK_DIR
mkdir $SPARK
mv $SPARK_DIR/* $SPARK
echo "localhost" >> $SPARK/conf/slaves
echo "SPARK_LOCAL_IP=localhost\nSPARK_MASTER_HOST=localhost" >> $SPARK/conf/spark-env.sh

$SPARK/sbin/start-all.sh

## INSTALLING KAFKA
cd resources
git clone https://github.com/wurstmeister/kafka-docker.git
