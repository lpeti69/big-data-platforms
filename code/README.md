# Code

## Prerequisites
- docker
- docker-compose
- pip
- pip3

## Setup

./setup.sh

## Run

./run_streaming_platform.sh

