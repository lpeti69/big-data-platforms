# This your assignment deployment report

it is a free form. you can add:

how to deploy your system: see code/README

2.1
Explain the implemented structures of the input streaming data and the output result, and the data serialization/deserialization, for the streaming analytics application (customerstreamapp) for customers.

Data serialization: 
Sensor -> Broker : Json
Broker -> Streaming : Json
Streaming -> Broker: Json
Broker -> Analytics App: Json
Sensor, Streaming, Analytics -> Log : Json/Text

Input streaming data:
Periodically extract records extracted from the sensor dataset csv file
and sent to one of the brokers in the kafka cluster

Output result:
Json containing statistical information regarding each client. In the world scenario each sensor user would only receive his/her own data separately through different topics.

2.2 
Explain the key logic of functions for processing events/records in customerstreamapp in your implementation.

The pipeline is the following:
1. Create spark driver with context
2. Create a kafka producer which will send the analytics back to the customer
3. Create a directed stream to a kafka broker, from which we expect the sensor data
4. Extract the location changes of each user from the stream (in analyze fun) in microbatching
5. Maintain a sliding window of statistics about the users including the distribution of its location changes recently (with update_room, update_timing).
6. Send the results back with a callback to the same kafka broker (handler)

2.3
Run customerstreamapp and show the operation of the customerstreamapp with your test environments. Explain the test environments. Discuss the analytics and its performance observations.

It records the number of entities/sensor records it can send during a given time period. It is slow, because the dataset is small, therefore I put an artificial sleep in the sending part.

Records ingested: 100 in 10 seconds
Records ingested: 100 in 10 seconds
Records ingested: 100 in 10 seconds
Records ingested: 99 in 10 seconds
Records ingested: 99 in 10 seconds
Records ingested: 100 in 10 seconds
Records ingested: 100 in 10 seconds
Records ingested: 99 in 10 seconds
Records ingested: 99 in 10 seconds
Records ingested: 100 in 10 seconds
Records ingested: 99 in 10 seconds
Records ingested: 100 in 10 seconds
Records ingested: 100 in 10 seconds
Records ingested: 99 in 10 seconds
Records ingested: 99 in 10 seconds
Records ingested: 100 in 10 seconds
Records ingested: 99 in 10 seconds
Records ingested: 100 in 10 seconds
Records ingested: 100 in 10 seconds
Records ingested: 99 in 10 seconds

I have a customer_analytics_app_emulator.py file, which emualtes an android application running on the client"s mobile phone. It connects to kafka and fetches the analytics result from it which then would be shown on the mobile app to the customer near real-time.

I save on average how much time does each customer spend at one place and accumulate how many times did she/he enter a certain place. They are identified by their names. Based on the latter which place is the favourite one for each customer. The time values are not similar to the real-world use cases since we send the data way more frequently from the csv. The window size is 15 update by default.
[
  {
    "1088": 0.10555555555555556,
    "1089": 0.3,
    "1090": 0.14285714285714285,
    "1091": 0.5625,
    "1092": 0.3214285714285714,
    "1094": 0.2222222222222222,
    "1095": 0.6666666666666666,
    "1101": 0.5500121585906498,
    "1104": 0.1111111111111111
  },
  {
    "1088": {
      "yard": 2,
      "summary": 0.5,
      "bedroom": 3,
      "livingroom": 8,
      "hall": 3,
      "kitchen": 3
    },
    "1089": {
      "bedroom": 4,
      "livingroom": 5,
      "hall": 2,
      "kitchen": 1,
      "summary": 0.2
    },
    "1090": {
      "bedroom": 1,
      "kitchen": 2,
      "hall": 4,
      "summary": 0.25
    },
    "1091": {
      "bathroom": 1,
      "yard": 1,
      "summary": 0.5,
      "bedroom": 2,
      "hall": 2,
      "kitchen": 3
    },
    "1092": {
      "bathroom": 1,
      "yard": 1,
      "summary": 0.5,
      "bedroom": 2,
      "hall": 1,
      "kitchen": 3
    },
    "1094": {
      "bathroom": 1,
      "livingroom": 6,
      "summary": 0.3333333333333333,
      "yard": 1,
      "hall": 2,
      "kitchen": 2
    },
    "1095": {
      "yard": 2,
      "livingroom": 2,
      "summary": 0.6666666666666666
    },
    "1101": {
      "bathroom": 5,
      "livingroom": 21,
      "summary": 2.5714285714285716,
      "bedroom": 52,
      "yard": 27,
      "hall": 1,
      "kitchen": 18
    },
    "1104": {
      "bathroom": 6,
      "yard": 2,
      "summary": 0.5714285714285714,
      "bedroom": 2,
      "livingroom": 1,
      "hall": 3,
      "kitchen": 4
    }
  },
  {
    "1088": 0.10555555555555556,
    "1089": 0.3,
    "1090": 0.14285714285714285,
    "1091": 0.5625,
    "1092": 0.3214285714285714,
    "1094": 0.2222222222222222,
    "1095": 0.6666666666666666,
    "1101": 0.5500121585906498,
    "1104": 0.10555555555555556
  },
  {
    "1088": {
      "yard": 2,
      "summary": 0.5,
      "bedroom": 3,
      "livingroom": 8,
      "hall": 3,
      "kitchen": 3
    },
    "1089": {
      "bedroom": 4,
      "livingroom": 5,
      "hall": 2,
      "kitchen": 1,
      "summary": 0.2
    },
    "1090": {
      "bedroom": 1,
      "kitchen": 2,
      "hall": 4,
      "summary": 0.25
    },
    "1091": {
      "bathroom": 1,
      "yard": 1,
      "summary": 0.5,
      "bedroom": 2,
      "hall": 2,
      "kitchen": 3
    },
    "1092": {
      "bathroom": 1,
      "yard": 1,
      "summary": 0.5,
      "bedroom": 2,
      "hall": 1,
      "kitchen": 3
    },
    "1094": {
      "bathroom": 1,
      "livingroom": 6,
      "summary": 0.3333333333333333,
      "yard": 1,
      "hall": 2,
      "kitchen": 2
    },
    "1095": {
      "yard": 2,
      "livingroom": 2,
      "summary": 0.6666666666666666
    },
    "1101": {
      "bathroom": 5,
      "livingroom": 21,
      "summary": 2.5714285714285716,
      "bedroom": 52,
      "yard": 27,
      "hall": 1,
      "kitchen": 18
    },
    "1104": {
      "bathroom": 11,
      "yard": 2,
      "summary": 1.1428571428571428,
      "bedroom": 3,
      "livingroom": 1,
      "hall": 3,
      "kitchen": 8
    }
  }
]
Thirdly, I have setup a customerstreamapp.py, which does not know about its environment (it does not have to setup the spark master url and it also gets the ip and port address of the kafka broker). By this, I emulate as if it would be provided by an external user, whom now does not have to care about the platform dependencies.

Logs how much time pases between consequetive analytics update. It is also  somewhat artificial due to the batch intervalls.

Elapsed time: 0.335079908371s
Elapsed time: 0.503800153732s
Elapsed time: 0.247876882553s
Elapsed time: 0.333935976028s
Elapsed time: 0.296519994736s
Elapsed time: 0.376636981964s
Elapsed time: 0.267571926117s
Elapsed time: 0.404363870621s
Elapsed time: 0.231199026108s
Elapsed time: 0.49981212616s
Elapsed time: 0.216072797775s
Elapsed time: 0.745835781097s
Elapsed time: 0.261830091476s
Elapsed time: 0.749666929245s
Elapsed time: 0.209583997726s
Elapsed time: 0.771173000336s
Elapsed time: 0.19975399971s
Elapsed time: 0.820001125336s
Elapsed time: 0.222444057465s
Elapsed time: 0.748003005981s
Elapsed time: 0.203197956085s
Elapsed time: 0.808268070221s
Elapsed time: 0.2229180336s
Elapsed time: 0.770882129669s
Elapsed time: 0.216954946518s
Elapsed time: 0.770477056503s
Elapsed time: 0.191522836685s
Elapsed time: 0.81859588623s
Elapsed time: 0.217732906342s



2.4
Present your tests and explain them for the situation in which wrong data is sent from or is
within data sources. Report how your implementation deals with that (e.g., exceptions,
failures, and decreasing performance). You should test with different error rates.

I have somewhat covered the tests in the previous point. There is no implementation dealing with a variety of different data sources, we expect, since it is a tightly coupled app, that the customer is able to extract json from the broker. From the sensor side we can easily achieve this, since we can design the interface. Consequently I have not made any error checks, especially since the streaming analytics is directly connected to the broker, meaning there is no data format tranformation required and we assume that the sensor provider can always provide default values in case of error. Otherwise we would have to check if the incoming message from the queue has the specific attributes and warning to the customer in case some of them are missing.

2.5
Explain parallelism settings in your implementation and test with different (higher) degrees of
parallelism. Report the performance and issues you have observed in your testing
environments. 

Since the dataset is not that large I have not implemented pararelism for the streaming analytics (it's the customer choice), but I have created a kafka cluster which is capable of handling large amount of data.