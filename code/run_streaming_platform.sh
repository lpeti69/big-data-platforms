#!/bin/bash

SPARK=resources/spark
SPARK_MASTER=localhost

PUBLIC_IP_ADDR=$(curl ifconfig.me)
export HOST_IP_ADDR=$PUBLIC_IP_ADDR

## RUNNING KAFKA
if ! echo $(docker ps) | grep 'kafka'; then
  cd resources/kafka-docker
  docker-compose up -d 
  docker-compose scale kafka=3
  cd ../..
fi

RUNNING_KAFKA_CONTAINER_NAME=$(sudo docker ps --format "{{.Names}}" | grep _kafka_ | head -1)
EXPOSED_PORT=$(docker inspect --format='{{(index (index .NetworkSettings.Ports "9092/tcp") 0).HostPort}}' $RUNNING_KAFKA_CONTAINER_NAME)

## RUNNING SENSOR EMULATION
python3 sensor.py $PUBLIC_IP_ADDR $EXPOSED_PORT &

## RUNNING EACH CLIENT'S JOB
/home/lpeti69/spark/bin/spark-submit \
    --deploy-mode=client \
    --master=spark://$SPARK_MASTER:7077 \
    --packages org.apache.spark:spark-streaming-kafka-0-8_2.11:2.4.4 \
      customerstreamapp.py $PUBLIC_IP_ADDR $EXPOSED_PORT &

#$SPARK/bin/spark-submit \
#    --deploy-mode client \
#    --master spark://$SPARK_MASTER:7077 \
#    --packages org.apache.spark:spark-streaming-kafka-0-8_2.11:2.4.4 \
#     $PUBLIC_IP_ADDR $EXPOSED_PORT customerstreamapp2.py


## RUNNING EACH CLIENT'S APP
python3 customer_analytics_app_emulator.py $PUBLIC_IP_ADDR $EXPOSED_PORT